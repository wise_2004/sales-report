SELECT
    week_number,
    rental_date AS sales_date,
    sales_amount,
    CUM_SUM,
    CENTERED_3_DAY_AVG
FROM (
    SELECT
        EXTRACT(WEEK FROM rental_date) AS week_number,
        rental_date,
        COUNT(*) AS sales_amount,
        SUM(COUNT(*)) OVER (ORDER BY EXTRACT(WEEK FROM rental_date), rental_date) AS CUM_SUM,
        AVG(COUNT(*)) OVER (ORDER BY EXTRACT(WEEK FROM rental_date), rental_date ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING) AS CENTERED_3_DAY_AVG
    FROM
        rental
    WHERE
        (EXTRACT(WEEK FROM rental_date) = 49 AND rental_date >= '1999-12-06') OR
        (EXTRACT(WEEK FROM rental_date) = 50) OR
        (EXTRACT(WEEK FROM rental_date) = 51 AND rental_date <= '1999-12-26')
    GROUP BY
        EXTRACT(WEEK FROM rental_date),
        rental_date
) AS WeeklySales;
